"use client";
import Image from "next/image";
import NavbarMenu from "./NavbarMenu";
import Headroom from "react-headroom";

function Navbar() {
  return (
    <Headroom>
      <nav className="py-4 px-28 flex md:flex-row flex-col md:justify-between justify-center items-center bg-[#fb6028dd] backdrop-blur-sm">
        <div>
          <Image
            src="/img/logo.png"
            alt="Suitmedia Logo"
            width={369}
            height={168}
            className="object-contain h-16 w-auto filter brightness-105"
          />
        </div>
        <NavbarMenu />
      </nav>
    </Headroom>
  );
}

export default Navbar;
