const getIdeasMeta = async (query: string) => {
  try {
    const res = await fetch(
      "https://suitmedia-backend.suitdev.com/api/ideas?" + query,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        cache: "no-cache",
      }
    );
    const data = await res.json();
    if (!data) throw new Error("No data found");
    return data;
  } catch (error) {
    console.log("🚀 ~ file: ideas.ts:19 ~ getIdeasMeta ~ error:", error);
  }
};

export { getIdeasMeta };
