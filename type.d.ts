type Sort = "-published_at" | "published_at";

type Image = {
  id: number;
  mime: string;
  file_name: string;
  url: string;
};

type Post = {
  id: number;
  title: string;
  slug: string;
  content: string;
  published_at: string;
  created_at: string;
  updated_at: string;
  deleted_at: string;
  small_image?: Image[];
  medium_image?: Image[];
};
